# Étude des applications Bag-of-Tasks du méso-centre Gricad


## The data

The dataset is available in Zenodo: https://zenodo.org/record/6787030


## ComPAS Paper

The paper is available in Hal: https://hal.archives-ouvertes.fr/hal-03702246

## Running the Analysis

### Enter the Nix Environment

```sh
$ nix develop
```

### Run the workflow

```sh
$ snakemake --cores 4 all
```

(you can change the number of total cores you wish)

### Artifacts

The produced `csv`s will be in the `data` folder.

The produced images will be in the `figs` folder.

