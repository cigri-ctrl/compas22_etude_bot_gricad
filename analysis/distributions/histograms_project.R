library(tidyverse)

args = commandArgs(trailingOnly=TRUE)
filename = args[1]
outname = args[2]

df <- read_csv(filename, col_names = T)

nb_cols <- 5
nb_rows <- 2

top_projects <- df %>%
    group_by(project) %>%
    summarise(n = n()) %>%
    arrange(desc(n)) %>%
    top_n(nb_rows * nb_cols) %>%
    pull(project)
    #mutate(id = cur_group_id()) %>%
    #ungroup() %>%

df %>%
    filter(project %in% top_projects) %>%
    group_by(project) %>%
    mutate(n = n()) %>%
    ungroup() %>%
    mutate(project_name = fct_reorder(project, n, .desc = TRUE)) %>%
    mutate(max_duration = max(duration)) %>%
    ggplot(aes(x = duration, fill = cluster_name)) +
    geom_histogram(bins = 20) + #bins = 20) +
    facet_wrap(.~ project_name, scales = "free", ncol = nb_cols) +
    xlab("Temps d'execution en echelle logarithmique (s)") +
    ylab("Compte") +
    xlim(0, NA) +
    ggtitle(paste("Histogramme des temps d'exécution des tâches des", nb_cols * nb_rows, "projets avec le plus de tâches")) +
    theme_bw() +
    theme(legend.position = "bottom", legend.box = "horizontal") +
    scale_x_log10(breaks=c(1, 10, 60, 10*60, 60*60, 10*60*60),labels=c("1s","10s","1m", "10m", "1h", "10h")) +
    scale_fill_grey(
      name = "Grappe",
      start = 0.2,
      end = 0.8,
      na.value = "red",
      aesthetics = "fill"
    )

ggsave(outname, width=10, height=5)
