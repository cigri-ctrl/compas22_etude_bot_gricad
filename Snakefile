
CC = "nix develop .#rshell --command Rscript"
data_raw = "data/data_cigri_2013_2023_raw.csv"
data = "data/data_cigri_2013_2023.csv"
data_filtered = "data/data_cigri_filtered.csv"
data_clusters = "data/cigri_clusters.csv"
data_campaigns = "data/campaign_props.csv"

# ---------------------------------------------------------------------------------------------------------------------

FILES_CSV = [
    # "distributions/fitting",

    "data_das",

    "temporal_analysis/temporal_evolution_data_global",
    "temporal_analysis/temporal_evolution_data_projects"
]

FILES_PDF = [
    "distributions/frechet_biggnss",
    "distributions/histograms_project",
    "distributions/cvm_example",

    "campaigns/ecdf_duration",
    "campaigns/ecdf_nb_jobs",
    "campaigns/nb_jobs_vs_duration",
    "campaigns/work",
    "campaigns/work_distrib",
    "campaigns/nb_job_distrib",
    "campaigns/rapport_distrib",

    "global/ecdf_comparison",
    "global/proportion_project_temporal",

    "temporal_analysis/global/temporal_evolution_nb_jobs_global",
    "temporal_analysis/global/temporal_evolution_duration_global",
    "temporal_analysis/global/temporal_evolution_campaign",
    "temporal_analysis/projects/temporal_evolution_duration",
    "temporal_analysis/projects/temporal_evolution_nb_jobs"
]

rule all:
    input:
        expand(["data/{file}.csv"], file=FILES_CSV),
        expand(["figs/{file}.pdf"], file=FILES_PDF)

# ---------------------------------------------------------------------------------------------------------------------

# rule download_data:
    # input:
        # "checkmd5.md5"
    # output:
        # data
    # shell:
        # "wget https://zenodo.org/record/6787030/files/data_cigri_2017_2021.csv -O {output} && md5sum -c {input}"
 
rule clean_data:
    input:
        "analysis/merge_csv_cigri.R",
        data_raw,
        data_clusters,
        data_campaigns
    output:
        data
    shell:
        "{CC} {input} {output}"

rule filter_data:
    input:
        "analysis/data_filter.R",
        data
    output:
        data_filtered
    shell:
        "{CC} {input} {output}"

rule das:
    input:
        "analysis/das/das.R",
        "data/gwa_t_1_das2.zip"
    output:
        "data/data_das.csv"
    shell:
        "{CC} {input} {output}"


# ---------------------------------------------------------------------------------------------------------------------


rule run_temporal_analysis_csv:
    input:
        "analysis/temporal_analysis/to_csv/{script}.R",
        data
    output:
        "data/temporal_analysis/{script}.csv"
    shell:
        "{CC} {input} {output}"

rule run_temporal_analysis_pdf:
    input:
        "analysis/temporal_analysis/to_pdf/{locality}/{script}.R",
        "data/temporal_analysis/temporal_evolution_data_{locality}.csv"
    output:
        "figs/temporal_analysis/{locality}/{script}.pdf"
    shell:
        "{CC} {input} {output}"

rule run_temporal_analysis_campaign:
    input:
        "analysis/temporal_analysis/to_pdf/global/temporal_evolution_campaign.R",
        data
    output:
        "figs/temporal_analysis/global/temporal_evolution_campaign.pdf"
    shell:
        "{CC} {input} {output}"

# ---------------------------------------------------------------------------------------------------------------------

rule distributions:
    input:
        "analysis/distributions/{script}.R",
        data_filtered
    output:
        "{folder}/distributions/{script}.{format}"
    shell:
        "{CC} {input} {output}"

# ---------------------------------------------------------------------------------------------------------------------

rule campaigns:
    input:
        "analysis/campaigns/{script}.R",
        data_filtered
    output:
        "{folder}/campaigns/{script}.{format}"
    shell:
        "{CC} {input} {output}"

# ---------------------------------------------------------------------------------------------------------------------

rule ecdf_global:
    input:
        "analysis/global/ecdf_comparison.R",
        data_filtered,
        "data/data_das.csv"
    output:
        "figs/global/ecdf_comparison.pdf"
    shell:
        "{CC} {input} {output}"

rule proportion_project:
    input:
        "analysis/global/proportion_project_temporal.R",
        data
    output:
        "figs/global/proportion_project_temporal.pdf"
    shell:
        "{CC} {input} {output}"


